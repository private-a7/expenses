﻿using Expenses.Model;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq.Expressions;

namespace Expenses.Repository
{
    public interface IRepository<T> where T:EntityBase
    {
        T Get(int id);
        void Save(T entity);
        void Delete(T entity);
        IEnumerable<T> GetAll();
        IEnumerable<T> GetAll(Expression<Func<T, bool>> conditionLambda);
    }
}
