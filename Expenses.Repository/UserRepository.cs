﻿using Expenses.Model;
using System;
using System.Collections.Generic;
using System.Text;

namespace Expenses.Repository
{
    public interface IUserRepository:IRepository<User>
    {
        public User GetUser(string email);
    }
}
