﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Expenses.Model
{
    public abstract class EntityBase
    {
        public const int NoneId = 0;
        public int Id { get; set; } = NoneId;
    }
}
