﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Expenses.Model
{
    public class PaymentItem : EntityBase
    {
        public double Amount { get; set; }
        public string Description { get; set; }



        public int PaymentId { get; set; }
        public Payment Payment { get; set; }
    }
}
