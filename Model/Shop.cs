﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Expenses.Model
{
    public class Shop : EntityBase
    {
        public string Name { get; set; }
        public string Description { get; set; }


        public int GroupId { get; set; }
        public PaymentGroup Group { get; set; }
    }
}
