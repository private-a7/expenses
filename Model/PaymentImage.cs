﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Expenses.Model
{
    public class PaymentImage : EntityBase
    {
        public string Name { get; set; }
        [MaxLength(36)]
        public string UniqueName { get; set; }
    }
}
