﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Expenses.Model
{
    public class Limit : EntityBase
    {
        /// <summary>
        /// Date when it is set. But, need to calculate 
        /// start period of date range, where it belongs to
        /// (if range starts on 1st, and this is 8th, it is still same month).
        /// </summary>
        public DateTime StartDate { get; set; }
        public double BottomAmount { get; set; }
        public double TopAmount { get; set; }


        public int UserId { get; set; }
        public User User { get; set; }
    }
}
