﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Expenses.Model
{
    public class PaymentComment : EntityBase
    {
        public string Comment { get; set; }

    }
}
