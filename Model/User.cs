﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text.RegularExpressions;

namespace Expenses.Model
{
    public class User : EntityBase
    {

        [Required]
        public string Email { get; set; }

        public ICollection<PaymentMethod> Methods { get; set; }

        public ICollection<PaymentGroup> PaymentGroup { get; set; }

        public ICollection<Limit> Limit { get; set; }

        public ICollection<PaymentTag> Tags { get; set; }

        /// <summary>
        /// Start day of the month (start at 1).
        /// 15.12.-12.01. is January. 16.01.-15.02. is February.
        /// </summary>
        public int FirstDayInMonth { get; set; } = 1;

        /// <summary>
        /// Returns limit which belongs to date range.
        /// </summary>
        /// <param name="start"></param>
        /// <param name="end"></param>
        /// <returns></returns>
        public Limit GetLimit(DateTime start, DateTime end)
        {
            return Limit.Where(x => start <= x.StartDate && x.StartDate < end).FirstOrDefault();
        }

        /// <summary>
        /// Returns first limit before given date.
        /// Because if limit is not set, previous one is used.
        /// </summary>
        /// <param name="start"></param>
        /// <returns></returns>
        public Limit GetPreviousLimit(DateTime start)
        {
            var ls = Limit
                .Where(x => x.StartDate <= start)
                .OrderBy(x=>x.StartDate)
                .LastOrDefault();
            return ls;

        }
    }
}
