﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Expenses.Model
{
    public class Payment : EntityBase
    {
        public double TotalAmount { get; set; }
        public DateTime PurchaseDate { get; set; }
        
        public int PaymentMethodId { get; set; }
        public PaymentMethod PaymentMethod { get; set; }

        public ICollection<PaymentImage> Images { get; set; }

        public ICollection<PaymentItem> Items { get; set; }

        public int? PaymentCommentId { get; set; }
        public PaymentComment PaymentComment { get; set; }

        public ICollection<PaymentTag> Tags { get; set; }

        public int? ShopId { get; set; }
        public Shop Shop { get; set; }


        public int UserId { get; set; }
    }
}
