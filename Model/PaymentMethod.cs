﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Expenses.Model
{
    public class PaymentMethod : EntityBase
    {
        public string Method { get; set; }
    }
}
