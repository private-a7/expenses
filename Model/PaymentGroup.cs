﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Text;

namespace Expenses.Model
{
    public class PaymentGroup : EntityBase
    {
        public string Name { get; set; }
        public ICollection<Shop> Shops { get; set; }
        /// <summary>
        /// ARGB value.
        /// </summary>
        public int Color { get; set; }


        public int UserId { get; set; }
        public User User { get; set; }
    }
}
