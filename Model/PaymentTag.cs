﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Expenses.Model
{
    public class PaymentTag : EntityBase
    {
        public string Tag { get; set; }
    }
}
