using Expenses.Model;
using Expenses.MySQLRepository;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;

namespace RepositoryTests
{
    public class PaymentRepository_Test
    {
        private string[] groupNames = new string[] { "group1", "group 2", "group 3" };
        private string[] shopNames = new string[] { "shop1", "shop 2", "shop 3" };
        private string userEmail = "email@example.com";
        private DbContextOptions<ApplicationDbContext> options;
        [OneTimeSetUp]
        public void SetUp()
        {
            string connection = "Database=ExpensesData_Test;Data Source=192.168.0.18;Port=3306;Uid=admin;Pwd=13579;";
            options = new DbContextOptionsBuilder<ApplicationDbContext>()
            //.UseInMemoryDatabase("IN_MEMORY_DATABASE")
            .UseMySql(connection)
            .Options;

            using (var context = new ApplicationDbContext(options))
            {
                try
                {
                    context.Database.EnsureDeleted();
                    context.Database.EnsureCreated();
                    User u = new User() { Email = userEmail };
                    UserRepository uR = new UserRepository(context);
                    uR.Save(u);
                    GroupRepository r = new GroupRepository(context);
                    Shop sh = new Shop() { Name = shopNames[0] };
                    PaymentGroup gr = new PaymentGroup() { User = u, Name = groupNames[0], Shops = new List<Shop>() { sh } };
                    r.Save(gr);
                }
                catch (Exception e)
                {
                    throw e;
                }
                finally
                {
                    //CleanupTestDatabase(context);
                }
            }
        }

        [Test]
        public void Save_Test_SavedCorrectly()
        {
            Payment p = new Payment()
            {
                Items = new List<PaymentItem>() { new PaymentItem() { Amount = 25, Description = "none" } },
            };
            string comment = "ovo je komentar";
            using (var context = new ApplicationDbContext(options))
            {
                UserRepository uR = new UserRepository(context);
                PaymentRepository r = new PaymentRepository(context);
                GroupRepository gR = new GroupRepository(context);

                User u = uR.GetUser(userEmail);
                PaymentMethod pm = new PaymentMethod() { Method = "kes" };
                context.PaymentMethods.Add(pm);
                p.PaymentMethod = pm;
                p.UserId = u.Id;
                p.Shop = gR.GetGroup(u.Id, groupNames[0]).Shops.First();
                p.PaymentComment = new PaymentComment() { Comment = comment };
                p.TotalAmount = 120;
                r.Save(p);

                context.SaveChanges();
            }
            using (var context = new ApplicationDbContext(options))
            {
                UserRepository uR = new UserRepository(context);
                PaymentRepository r = new PaymentRepository(context);
                GroupRepository gR = new GroupRepository(context);

                User u = uR.GetUser(userEmail);
                var payments = r.GetAllForUser(u.Id);
                Assert.AreEqual(1, payments.Count());

                var pp = payments.First();
                Assert.AreEqual(120, pp.TotalAmount);
                Assert.NotNull(pp.PaymentComment);
                Assert.AreEqual(comment, pp.PaymentComment.Comment);


            }
        }

        [Test]
        public void Delete_Test_None_Left()
        {
            Payment p = new Payment()
            {
                Items = new List<PaymentItem>() { new PaymentItem() { Amount = 25, Description = "none" } },
            };
            string comment = "ovo je komentar";
            using (var context = new ApplicationDbContext(options))
            {
                UserRepository uR = new UserRepository(context);
                PaymentRepository r = new PaymentRepository(context);
                GroupRepository gR = new GroupRepository(context);

                User u = uR.GetUser(userEmail);
                PaymentMethod pm = new PaymentMethod() { Method = "kes" };
                context.PaymentMethods.Add(pm);
                p.PaymentMethod = pm;
                p.UserId = u.Id;
                p.Shop = gR.GetGroup(u.Id, groupNames[0]).Shops.First();
                p.PaymentComment = new PaymentComment() { Comment = comment };
                p.TotalAmount = p.Items.Sum(x=>x.Amount);
                r.Save(p);

                context.SaveChanges();
            }
            using (var context = new ApplicationDbContext(options))
            {
                PaymentRepository r = new PaymentRepository(context);
                UserRepository uR = new UserRepository(context);
                User u = uR.GetUser(userEmail);
                var payments = r.GetAllForUser(u.Id);
                Assert.AreEqual(1, payments.Count());

                var pp = payments.First();
                r.Delete(pp);
                context.SaveChanges();

            }
            using (var context = new ApplicationDbContext(options))
            {
                UserRepository uR = new UserRepository(context);
                PaymentRepository r = new PaymentRepository(context);
                GroupRepository gR = new GroupRepository(context);

                User u = uR.GetUser(userEmail);
                var payments = r.GetAllForUser(u.Id);
                Assert.IsFalse(payments.Any());

                var comments = context.PaymentComments.ToList();
                Assert.IsFalse(comments.Any(), "comment not deleted");

                var items = context.PaymentItems.ToList();
                Assert.IsFalse(items.Any(), "items not deleted");

            }
        }
    }
}