using Expenses.Model;
using Expenses.MySQLRepository;
using Microsoft.EntityFrameworkCore;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace RepositoryTests
{
    public class UserRepository_Test
    {
        private DbContextOptions<ApplicationDbContext> options;
        [OneTimeSetUp]
        public void SetUp()
        {
            string connection = "Database=ExpensesData_Test;Data Source=192.168.0.18;Port=3306;Uid=admin;Pwd=13579;";
            options = new DbContextOptionsBuilder<ApplicationDbContext>()
            //.UseInMemoryDatabase("IN_MEMORY_DATABASE")
            .UseMySql(connection)
            .Options;

            using (var context = new ApplicationDbContext(options))
            {
                try
                {
                    context.Database.EnsureDeleted();
                    context.Database.EnsureCreated();
                }
                catch (Exception e)
                {
                    throw e;
                }
                finally
                {
                    //CleanupTestDatabase(context);
                }
            }
        }

        [Test]
        public void Save_Test()
        {
            User u = new User()
            {
                Email= "test@example.com",
                PaymentGroup = new List<PaymentGroup>() 
                { 
                    new PaymentGroup() { Name = "default" } ,
                    new PaymentGroup() { Name = "group 1" } 
                }
            };
            using(var context = new ApplicationDbContext(options))
            {
                UserRepository r = new UserRepository(context);
                r.Save(u);
            }
        }
    }
}