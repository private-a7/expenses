﻿using Expenses.Model;
using Expenses.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Expenses.MySQLRepository
{
    public interface IPaymentRepository:IRepository<Payment>
    {
        IEnumerable<Payment> GetAllForUser(int userId);
        IEnumerable<Payment> GetAllForUser(int userId, DateTime startDate, DateTime? endDate = null);

        void DeleteComment(int id);
    }
    public class PaymentRepository:IPaymentRepository
    {
        private ApplicationDbContext context { get; set; }
        public PaymentRepository(ApplicationDbContext ctx) => context = ctx;

        public void Delete(Payment entity)
        {
            if (entity.PaymentComment != null)
                context.Remove(entity.PaymentComment);
            context.Remove(entity);
            context.SaveChanges();
        }
        public void DeleteComment(int id)
        {
            var comm = context.PaymentComments.Find(id);
            if (comm != null)
                context.PaymentComments.Remove(comm);
        }

        public IEnumerable<Payment> GetAllForUser(int userId)
        {
            return context.Payments.Where(x => x.UserId == userId)
                .Include(x => x.PaymentMethod)
                .Include(x => x.PaymentComment)
                .Include(x => x.Images)
                .Include(x => x.Items)
                .Include(x => x.Tags)
                .ToList();
        }
        public IEnumerable<Payment> GetAllForUser(int userId, DateTime startDate, DateTime? endDate = null)
        {
            Expression<Func<Payment, bool>> check = p => true;
            if (endDate != null && endDate > startDate)
                check = p => p.PurchaseDate <= endDate;

            return context.Payments.Where(x => x.UserId == userId && x.PurchaseDate > startDate)
                .Where(check)
                .Include(x => x.PaymentMethod)
                .Include(x => x.PaymentComment)
                .Include(x => x.Images)
                .Include(x => x.Items)
                .Include(x => x.Tags)
                .ToList();
        }
        public Payment Get(int id)
        {
            return context.Payments.Where(x => x.Id == id)
                .Include(x => x.PaymentMethod)
                .Include(x => x.PaymentComment)
                .Include(x => x.Images)
                .Include(x => x.Items)
                .Include(x => x.Tags)
                .FirstOrDefault();
        }

        public IEnumerable<Payment> GetAll()
        {
            return context.Payments
                .Include(x => x.PaymentMethod)
                .Include(x => x.PaymentComment)
                .Include(x => x.Images)
                .Include(x => x.Items)
                .Include(x => x.Tags)
                .ToArray();
        }

        public IEnumerable<Payment> GetAll(Expression<Func<Payment, bool>> conditionLambda)
        {
            return context.Payments
                .Include(x => x.PaymentMethod)
                .Include(x => x.PaymentComment)
                .Include(x => x.Images)
                .Include(x => x.Items)
                .Include(x => x.Tags)
                .Where(conditionLambda)
                .ToArray();
        }

        public void Save(Payment entity)
        {
            User user = context.Users.FirstOrDefault(x => x.Id == entity.UserId);
            if (user == null)
                throw new ArgumentException($"Payment is assigned to unexisting User <{entity.Id}>");
            if (entity.PaymentMethod == null)
                throw new NullReferenceException("PaymentMethod not assigned!");
            if (entity.Items == null || entity.Items.Count == 0)
                throw new NullReferenceException("Payment must have at least 1 entity!");

            PaymentMethod pm = user.Methods.FirstOrDefault(x => x.Method == entity.PaymentMethod.Method);
            if (pm == null)
            {
                user.Methods.Add(entity.PaymentMethod);
                context.Attach(user);
            }
            else
                context.Attach(pm);
            if (entity.PaymentComment != null)
            {
                if (entity.PaymentComment.Id == EntityBase.NoneId)
                    context.Add(entity.PaymentComment);
                else
                    context.Attach(entity.PaymentComment);
            }

            if (entity.Images != null)
            foreach (var img in entity.Images)
                if (img.Id == EntityBase.NoneId)
                    context.Add(img);
                else
                    context.Attach(img);
            foreach (var it in entity.Items)
                if (it.Id == EntityBase.NoneId)
                    context.Add(it);
                else
                    context.Attach(it);

            List<PaymentTag> tags = new List<PaymentTag>();
            bool updateUser = false;
            // Use existing tag if found, create new one if not.
            if (entity.Tags != null)
            foreach(var t in entity.Tags)
            {
                    if (user.Tags == null)
                        user.Tags = new List<PaymentTag>();
                PaymentTag existingTag = user.Tags.FirstOrDefault(x => x.Tag == t.Tag);
                if (existingTag == null)
                {
                    updateUser = true;
                    user.Tags.Add(t);
                    tags.Add(t);
                }
                else
                    tags.Add(existingTag);
            }
            if (updateUser)
                context.Attach(user);

            entity.Tags = tags;
            if (entity.Id == EntityBase.NoneId)
                context.Add(entity);
            else
                context.Attach(entity);
            context.SaveChanges();
        }
    }
}
