﻿using Expenses.Model;
using Expenses.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Expenses.MySQLRepository
{
    public class UserRepository : IUserRepository
    {
        private ApplicationDbContext context { get; set; }

        public UserRepository(ApplicationDbContext ctx)
        {
            context = ctx;
        }

        public void Delete(User entity)
        {
            //throw new NotImplementedException("Delete or just mark as deleted? ");
            var groups = context.PaymentGroups.Where(x => x.UserId == entity.Id).Include(x=>x.Shops).ToList();
            var allShops = groups.Where(x=>x.Shops != null).SelectMany(x => x.Shops).ToList();
            HashSet<int> shopIds = new HashSet<int>(allShops.Select(x => x.Id).Distinct());
            var payments = context.Payments.Where(x => x.UserId == entity.Id).ToList();
            payments = payments.Where(x => shopIds.Contains(x.ShopId.Value)).ToList();
            foreach (var p in payments)
                context.Remove(p);
            foreach (var s in allShops)
                context.Remove(s);
            context.PaymentGroups.RemoveRange(groups);
            context.SaveChanges();
        }

        public User Get(int id)
        {
            return context.Users.Where(x => x.Id == id)
                .Include(x=>x.Limit)
                .Include(x=>x.Methods)
                .Include(x=>x.PaymentGroup)
                .ThenInclude(x=>x.Shops)
                .FirstOrDefault();
        }
        public User GetUser(string email)
        {
            var user = context.Users.Where(x => x.Email == email).FirstOrDefault();
            if (user == null)
                return null;
            return Get(user.Id);
        }

        public IEnumerable<User> GetAll()
        {
            return context.Users
                .Include(x => x.Limit)
                .Include(x => x.Methods)
                .Include(x => x.PaymentGroup)
                .ThenInclude(x => x.Shops)
                .ToArray();
        }

        public IEnumerable<User> GetAll(Expression<Func<User, bool>> conditionLambda)
        {
            return context.Users
                .Include(x => x.Limit)
                .Include(x => x.Methods)
                .Include(x => x.PaymentGroup)
                .ThenInclude(x => x.Shops)
                .Where(conditionLambda)
                .ToArray();
        }

        /// <summary>
        /// Decided Add/Update. 
        /// </summary>
        /// <param name="entity"></param>
        public void Save(User entity)
        {
            if (entity.Methods != null)
            foreach (var pm in entity.Methods)
                if (pm.Id == EntityBase.NoneId)
                    context.Set<PaymentMethod>().Add(pm);
                else
                    context.Set<PaymentMethod>().Attach(pm);

            var limits = context.Limits.Where(x => x.UserId == entity.Id).ToArray();
            context.RemoveRange(limits);
            if (entity.Limit == null)
            {
                entity.Limit = new List<Limit>();
                entity.Limit.Add(new Limit()
                {
                    BottomAmount = 0,
                    StartDate = DateTime.Now,
                });
            }
            else
                foreach (var pl in entity.Limit)
                    if (pl.Id == EntityBase.NoneId)
                        context.Set<Limit>().Add(pl);
                    else
                        context.Set<Limit>().Attach(pl);

            if (entity.PaymentGroup != null)
            foreach (var gr in entity.PaymentGroup)
            {
                if (gr.Shops != null)
                foreach (var sh in gr.Shops)
                    if (sh.Id == EntityBase.NoneId)
                        context.Add(sh);
                    else
                        context.Attach(sh);

                if (gr.Id == EntityBase.NoneId)
                    context.Add(gr);
                else
                    context.Attach(gr);
            }

            if (entity.Tags != null)
            foreach(var t in entity.Tags)
            {
                context.Tags.AddRange(entity.Tags.Select(x => new PaymentTag() { Tag = x.Tag }));
                
            }

            if (entity.Id == EntityBase.NoneId)
                context.Add(entity);
            else
                context.Attach(entity);

            context.SaveChanges();
        }

    }
}
