﻿using Expenses.Model;
using Expenses.Repository;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Expenses.MySQLRepository
{
    public interface IGroupRepository:IRepository<PaymentGroup>
    {
        PaymentGroup GetGroup(int userId, string name);
        IEnumerable<PaymentGroup> GetGroups(int userId);
    }
    public class GroupRepository : IGroupRepository
    {
        private ApplicationDbContext context { get; set; }

        public GroupRepository(ApplicationDbContext ctx)
        {
            context = ctx;
        }

        public void Delete(PaymentGroup entity)
        {
            throw new NotImplementedException("Delete or just mark as deleted? ");
        }

        public PaymentGroup Get(int id)
        {
            return context.PaymentGroups.Where(x => x.Id == id)
                .Include(x=>x.Shops)
                .FirstOrDefault();
        }
        public IEnumerable<PaymentGroup> GetGroups(int userId)
        {
            return context.PaymentGroups
                .Where(x => x.UserId == userId)
                .ToArray();
        }

        public PaymentGroup GetGroup(int userId, string name)
        {
            PaymentGroup gr = context.PaymentGroups.FirstOrDefault(x => x.UserId == userId && x.Name == name);
            if (gr != null)
                return Get(gr.Id);
            return null;
        }

        public IEnumerable<PaymentGroup> GetAll()
        {
            return context.PaymentGroups
                .Include(x=>x.Shops)
                .ToArray();
        }

        public IEnumerable<PaymentGroup> GetAll(Expression<Func<PaymentGroup, bool>> conditionLambda)
        {
            return context.PaymentGroups
                .Include(x=>x.Shops)
                .Where(conditionLambda)
                .ToArray();
        }

        /// <summary>
        /// Decided Add/Update. 
        /// </summary>
        /// <param name="entity"></param>
        public void Save(PaymentGroup entity)
        {
            if (entity.User == null)
                throw new NullReferenceException("User not set for Group!");
            if (entity.Shops != null)
                foreach (var pm in entity.Shops)
                    if (pm.Id == EntityBase.NoneId)
                        context.Add(pm);
                    else
                        context.Attach(pm);

            if (entity.Id == EntityBase.NoneId)
                context.Add(entity);
            else
                context.Attach(entity);

            context.SaveChanges();
        }

    }
}
