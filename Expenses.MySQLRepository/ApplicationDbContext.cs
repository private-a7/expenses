﻿using Expenses.Model;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Expenses.MySQLRepository
{
    public class ApplicationDbContext:DbContext
    {
        public DbSet<User> Users { get; set; }
        public DbSet<PaymentMethod> PaymentMethods { get; set; }
        public DbSet<Limit> Limits { get; set; }
        public DbSet<PaymentGroup> PaymentGroups { get; set; }
        public DbSet<Shop> Shops { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<PaymentComment> PaymentComments { get; set; }
        public DbSet<PaymentImage> Images { get; set; }
        public DbSet<PaymentItem> PaymentItems { get; set; }
        public DbSet<PaymentTag> Tags { get; set; }


        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {
        }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
        }
    }
}
