using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Expenses.DataAccess;
using Expenses.Infrastructure;
using Expenses.Model;
using Expenses.Models.Data;
using Expenses.MySQLRepository;
using Expenses.Repository;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;

namespace Expenses
{
    public class Startup
    {
        public IConfiguration Configuration { get; }

        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }


        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            string connectionString = Configuration["SQL:DataConnectionString"];
            string identityConnectionString = Configuration["SQL:IdentityConnectionString"];
            services.AddDbContext<ApplicationDbContext>(options => 
            {
                options.UseMySql(connectionString, b=>b.MigrationsAssembly("Expenses"));
            });
            services.AddDbContext<AppIdentityDbContext>(options =>
            {
                options.UseMySql(identityConnectionString);
            });
            services.AddIdentity<AppUser, IdentityRole>(o =>
            {
                o.Password.RequireNonAlphanumeric = false;
                o.Password.RequiredLength = 4;
                o.Password.RequireUppercase = false;
            })
                .AddRoles<IdentityRole>()
            .AddEntityFrameworkStores<AppIdentityDbContext>()
            .AddDefaultTokenProviders();

            services.Configure<IdentityOptions>(opts =>
            {
                
            });
            services.ConfigureApplicationCookie(opts =>
            {
                opts.Cookie.HttpOnly = true;
                opts.ExpireTimeSpan = new TimeSpan(7, 0, 0, 0, 0);
                // Expiration time when no requests from user.
                //opts.ExpireTimeSpan = TimeSpan.FromMinutes(1);
                opts.LoginPath = "/Home/Login";
                opts.AccessDeniedPath = "/Home/Login";
                opts.SlidingExpiration = true;
            });

            services.AddTransient<IUserRepository, UserRepository>();
            services.AddTransient<IPaymentRepository, PaymentRepository>();
            services.AddTransient<IGroupRepository, GroupRepository>();

            

            services.AddControllersWithViews()
                // Needed so cshtml pages are updated on save. Depending on nuget package, starting from 
                // ASP.Net Core 3.
                
                .AddRazorRuntimeCompilation();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            //app.UseStaticFiles(new StaticFileOptions()
            //{
            //    FileProvider = new PhysicalFileProvider(Path.Combine(Directory.GetCurrentDirectory(), "wwwroot")),
            //    RequestPath = "/static"
            //});

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });

            //if (env.IsDevelopment())
            {
                //FakeData.Seed(app.ApplicationServices, Configuration);

                // Delete all users data
                using (var scope = app.ApplicationServices.CreateScope())
                {
                    var context = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
                    var idContext = scope.ServiceProvider.GetRequiredService<AppIdentityDbContext>();
                    //context.Database.EnsureDeleted();
                    //context.Database.Migrate();
                    //idContext.Database.Migrate();
                    
                    //context.Database.EnsureCreated();
                    //idContext.Database.EnsureCreated();

                    bool deleteAllUserData = false;
                    if (deleteAllUserData)
                    {
                        var userRepo = scope.ServiceProvider.GetRequiredService<IUserRepository>();
                        int userId = 1;
                        var user = context.Users.Where(x => x.Id == userId).FirstOrDefault();
                        userRepo.Delete(user);
                    }
                }
            }
            Initialization.Seed(app.ApplicationServices, Configuration);
        }
    }
}
