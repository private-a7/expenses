﻿using Expenses.Model;
using Expenses.Models.ImportExports.Model;
using Expenses.MySQLRepository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace Expenses.Models.ImportExports
{
    public class Export
    {
        public static string ToJson(IPaymentRepository paymentsRepo, User user)
        {
            JsonUser u = new JsonUser(user, paymentsRepo.GetAllForUser(user.Id));
            string json = JsonSerializer.Serialize(u);
            return json;
        }
    }
}
