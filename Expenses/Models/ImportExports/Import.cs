﻿using Expenses.Model;
using Expenses.Models.ImportExports.Model;
using Expenses.MySQLRepository;
using Expenses.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace Expenses.Models.ImportExports
{
    public class Import
    {
        public static void LoadFromJson(IPaymentRepository paymentsRepo, IUserRepository userRepo, 
            IGroupRepository groupRepo, string json)
        {
            var jUser = JsonSerializer.Deserialize<JsonUser>(json);
            User u = userRepo.GetUser(jUser.Email);
            if (u == null)
                throw new JsonException("User does not match!");

            u.FirstDayInMonth = jUser.StartDay;
            u.Limit.Clear();
            foreach(var jl in jUser.Limits)
            {
                Limit l = jl.ToLimit();
                u.Limit.Add(l);
            }

            u.PaymentGroup.Clear();
            foreach(var jg in jUser.Groups)
            {
                PaymentGroup g = jg.ToGroup();
                foreach(var js in jg.Shops)
                {
                    var s = js.ToShop();
                    g.Shops.Add(s);
                }
                groupRepo.Save(g);
                u.PaymentGroup.Add(g);
            }
            userRepo.Save(u);

            IEnumerable<Payment> payments = paymentsRepo.GetAllForUser(u.Id);
            foreach (var p in payments)
                paymentsRepo.Delete(p);

            foreach(var jp in jUser.Payments)
            {
                Payment p = jp.ToPayment();
                foreach(var ji in jp.Items)
                {
                    PaymentItem i = ji.ToPaymentItem();
                    p.Items.Add(i);
                }
                p.UserId = u.Id;
                p.Shop = u.PaymentGroup.First(x => x.Name == jp.Group).Shops.First(x => x.Name == jp.Shop);
                paymentsRepo.Save(p);
            }

            //userRepo.Save(u);

        }
    }
}
