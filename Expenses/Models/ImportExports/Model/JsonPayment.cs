﻿using Expenses.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Expenses.Models.ImportExports.Model
{
    public class JsonPayment
    {
        public DateTime Date { get; set; }
        public string Group { get; set; }
        public string Shop { get; set; }
        public string Method { get; set; }
        public IEnumerable<JsonItem> Items { get; set; }

        public JsonPayment() { }
        public JsonPayment(Payment p)
        {
            Date = p.PurchaseDate;
            Group = p.Shop.Group.Name;
            Shop = p.Shop.Name;
            Method = p.PaymentMethod.Method;
            Items = p.Items.Select(x => new JsonItem(x)).ToList();
        }

        public Payment ToPayment()
        {
            var m = new Payment()
            {
                PurchaseDate = Date
            };
            return m;
        }
    }
}
