﻿using Expenses.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Expenses.Models.ImportExports.Model
{
    public class JsonShop
    {
        public string Name { get; set; }

        public JsonShop() { }
        public JsonShop(Shop s)
        {
            Name = s.Name;
        }
        public Shop ToShop()
        {
            var s = new Shop()
            {
                Name = Name,
            };
            return s;
        }
    }
}
