﻿using Expenses.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Expenses.Models.ImportExports.Model
{
    public class JsonMethod
    {
        public string Name { get; set; }

        public JsonMethod() { }
        public JsonMethod(PaymentMethod m)
        {
            Name = m.Method;
        }

        public JsonMethod ToMethod()
        {
            var m = new JsonMethod()
            {
                Name = Name,
            };
            return m;
        }
    }
}
