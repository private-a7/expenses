﻿using Expenses.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Expenses.Models.ImportExports.Model
{
    public class JsonLimit
    {
        public DateTime Date { get; set; }
        public double TopLimit { get; set; }
        public double BottomLimit { get; set; }

        public JsonLimit() { }
        public JsonLimit(Limit l)
        {
            Date = l.StartDate;
            TopLimit = l.TopAmount;
            BottomLimit = l.BottomAmount;
        }
        public Limit ToLimit()
        {
            var l = new Limit()
            {
                StartDate = Date,
                TopAmount = TopLimit,
                BottomAmount = BottomLimit
            };
            return l;
        }
    }
}
