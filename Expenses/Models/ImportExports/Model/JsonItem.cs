﻿using Expenses.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Expenses.Models.ImportExports.Model
{
    public class JsonItem
    {
        public string Description { get; set; }
        public double Amount { get; set; }

        public JsonItem() { }
        public JsonItem(PaymentItem m)
        {
            Description = m.Description;
            Amount = m.Amount;
        }

        public PaymentItem ToPaymentItem()
        {
            var m = new PaymentItem()
            {
                Description = Description,
                Amount = Amount
            };
            return m;
        }
    }
}
