﻿using Expenses.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Expenses.Models.ImportExports.Model
{
    public class JsonGroup
    {
        public string Name { get; set; }
        public int Color { get; set; }
        public IEnumerable<JsonShop> Shops { get; set; }

        public JsonGroup() { }
        public JsonGroup(PaymentGroup g)
        {
            Name = g.Name;
            Color = g.Color;
            Shops = g.Shops.Select(x => new JsonShop(x)).ToList();
        }

        public PaymentGroup ToGroup()
        {
            var g = new PaymentGroup()
            {
                Name = Name,
                Color = Color
            };
            return g;
        }
    }
}
