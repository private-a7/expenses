﻿using Expenses.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Expenses.Models.ImportExports.Model
{
    public class JsonUser
    {
        public string Email { get; set; }
        public int Id { get; set; }
        public int StartDay { get; set; }
        public IEnumerable<JsonLimit> Limits { get; set; }
        public IEnumerable<JsonMethod> Methods { get; set; }
        public IEnumerable<JsonGroup> Groups { get; set; }
        public IEnumerable<JsonPayment> Payments { get; set; }

        public JsonUser() { }
        public JsonUser(User u, IEnumerable<Payment> userPayments)
        {
            Email = u.Email;
            StartDay = u.FirstDayInMonth;
            Limits = u.Limit.Select(x => new JsonLimit(x)).ToList();
            Methods = u.Methods.Select(x => new JsonMethod(x)).ToList();
            Groups = u.PaymentGroup.Select(x => new JsonGroup(x)).ToList();
            Payments = userPayments.Select(x => new JsonPayment(x)).ToList();
        }

    }
}
