﻿using Expenses.Model;
using Expenses.MySQLRepository;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace Expenses.Models
{
    public class ImportExport
    {
        public static string ToJson(IPaymentRepository paymentsRepo, User user)
        {
            var opts = new JsonWriterOptions()
            {
                Indented = true,
            };

            string json = "{}";
            using (var stream = new MemoryStream())
            {
                using (var wr = new Utf8JsonWriter(stream, opts))
                {
                    wr.WriteStartObject();
                    {
                        wr.WriteString("email", user.Email);
                        wr.WriteNumber("id", user.Id);
                        wr.WriteNumber("startday", user.FirstDayInMonth);

                        wr.WriteStartArray("limits");
                        {
                            foreach (var l in user.Limit)
                            {
                                wr.WriteStartObject();
                                {
                                    wr.WriteString("date", l.StartDate);
                                    wr.WriteNumber("top", l.TopAmount);
                                    wr.WriteNumber("bottom", l.BottomAmount);
                                }
                                wr.WriteEndObject();
                            }
                        }
                        wr.WriteEndArray();

                        wr.WriteStartArray("groups");
                        {
                            foreach (var g in user.PaymentGroup)
                            {
                                wr.WriteStartObject();
                                {
                                    wr.WriteString("name", g.Name);
                                    wr.WriteNumber("color", g.Color);
                                    wr.WriteStartArray("shops");
                                    {
                                        foreach (var s in g.Shops)
                                        {
                                            wr.WriteStartObject();
                                            {
                                                wr.WriteString("name", s.Name);
                                                wr.WriteNumber("id", s.Id);
                                            }
                                            wr.WriteEndObject();
                                        }
                                    }
                                    wr.WriteEndArray();
                                }
                                wr.WriteEndObject();
                            }
                        }
                        wr.WriteEndArray();

                        wr.WriteStartArray("methods");
                        {
                            foreach(var m in user.Methods)
                            {
                                wr.WriteStartObject();
                                {
                                    wr.WriteString("name", m.Method);
                                }
                                wr.WriteEndObject();
                            }
                        }
                        wr.WriteEndArray();

                        wr.WriteStartArray("payments");
                        {

                        }
                        wr.WriteEndArray();

                    }
                    // End of document.
                    wr.WriteEndObject();
                }
            }
            return json;
        }
    }
}
