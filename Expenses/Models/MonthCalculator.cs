﻿using Expenses.Models.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Expenses.Models
{
    public class MonthCalculator
    {
        /// <summary>
        /// Start month day is before SplitDay: 
        ///     - any given day before start month day belongs to month before.
        ///     - all other days belong to current month
        ///     - Example: start month day is 5. 4. of march belongs to february, 6. of march belongs to march.
        /// Start month day is equal or after SplitDay:
        ///     - any given day before start month day belongs to current month
        ///     - all other days belong to next month
        ///     - Example: start month day is 20. 4. of march belongs to march, 6. of march belongs to april.
        /// </summary>
        public static readonly int SplitDay = 15;
        /// <summary>
        /// Returns start date for period in which current date is.
        /// </summary>
        /// <param name="startDay">Day in month, 1-31</param>
        /// <param name="date"></param>
        /// <returns>00:00</returns>
        public static DateTime GetStartPeriod(int startDay, DateTime date)
        {
            int days = DateTime.DaysInMonth(date.Year, date.Month);
            if (days < startDay)
                startDay = days;

            int monthOffset = 0;
            if (date.Day < startDay)
                monthOffset = -1;

            DateTime dt = new DateTime(date.Year, date.Month, startDay, 0, 0, 0, 0);
            dt = dt.AddMonths(monthOffset);
            return dt;
        }

        /// <summary>
        /// Returns end date for period in which current date is.
        /// </summary>
        /// <param name="startDay">Day in month, 1-31</param>
        /// <param name="date"></param>
        /// <returns>23:59</returns>
        public static DateTime GetEndPeriod(int startDay, DateTime date)
        {
            DateTime dt = GetStartPeriod(startDay, date);
            dt = dt.AddMonths(1);
            dt = dt.AddMilliseconds(-1);
            return dt;
        }

        /// <summary>
        /// Returns month (1-12) to which date belongs to.
        /// </summary>
        /// <param name="startDay">Day in month, 1-31</param>
        /// <param name="date"></param>
        /// <returns></returns>
        public static int GetMonth(int startDay, DateTime date)
        {

            int monthOffset = 0;
            if (startDay < SplitDay)
            {
                if (date.Day < startDay)
                    monthOffset = -1;
            }
            else
            {
                if (startDay <= date.Day)
                    monthOffset = +1;
            }
            date = date.AddMonths(monthOffset);
            return date.Month;
        }

        /// <summary>
        /// Returns year to which date belongs to.
        /// </summary>
        /// <param name="startDay"></param>
        /// <param name="date"></param>
        /// <returns></returns>
        public static int GetYear(int startDay, DateTime date)
        {

            int monthOffset = 0;
            if (startDay < SplitDay)
            {
                if (date.Day < startDay)
                    monthOffset = -1;
            }
            else
            {
                if (startDay < date.Day)
                    monthOffset = +1;
            }
            date = date.AddMonths(monthOffset);
            return date.Year;
        }

        /// <summary>
        /// Returns start date for given year/month.
        /// </summary>
        /// <param name="startDay"></param>
        /// <param name="year"></param>
        /// <param name="month">1-12</param>
        /// <returns></returns>
        public static DateTime GetStartPeriod(int startDay, int year, int month)
        {
            int days = DateTime.DaysInMonth(year, month);
            if (days < startDay)
                startDay = days;

            // Why needed this?
            // If date set for month selection is 1.6, and start day is 10. then 
            // month number 5 will be returned, instead of 6.
            DateTime dt = new DateTime(year, month, startDay);
            // If startDay is after split day, this means it belongs to next month.
            if (startDay > SplitDay)
                dt = dt.AddDays(-1);
            return GetStartPeriod(startDay, dt);
        }
        /// <summary>
        /// Returns end date for given year/month.
        /// </summary>
        /// <param name="startDay"></param>
        /// <param name="year"></param>
        /// <param name="month">1-12</param>
        /// <returns></returns>
        public static DateTime GetEndPeriod(int startDay, int year, int month)
        {
            var dt = GetStartPeriod(startDay, year, month);
            return GetEndPeriod(startDay, dt);
        }
    }
}
