﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Expenses.Models.ViewModel
{
    public class GroupShortInfo
    {
        public string Name { get; set; }
        public System.Drawing.Color Color { get; set; }
        /// <summary>
        /// Spent money in group.
        /// </summary>
        public double Amount { get; set; }
    }
}
