﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Expenses.Models.ViewModel
{
    public class LimitModel
    {
        [Required]
        public int Id { get; set; }
        [Required]
        public DateTime Date { get; set; }
        [Required]
        public double TopLimit { get; set; }
        [Required]
        public double BottomLimit { get; set; }
    }
}
