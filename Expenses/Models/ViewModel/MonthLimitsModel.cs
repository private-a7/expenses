﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Expenses.Models.ViewModel
{
    public class MonthsModel
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public IEnumerable<MonthLimitsModel> Months { get; set; }
    }
    public class MonthLimitsModel
    {
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        /// <summary>
        /// 1-12.
        /// </summary>
        public int Month { get; set; }
        public int Year { get; set; }
        public double TopLimit { get; set; }
        public double BottomLimit { get; set; }
        public double Spent { get; set; }
    }
}
