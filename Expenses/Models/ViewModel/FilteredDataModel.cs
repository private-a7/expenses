﻿using Expenses.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Expenses.Models.ViewModel
{
    public class FilteredDataModel
    {
        /// <summary>
        /// Start day of month (that user selected).
        /// </summary>
        public int StartDay { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public IEnumerable<Payment> Payments { get; set; }
    }
}
