﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Expenses.Models.ViewModel
{
    public enum Month
    {
        January = 1,
        February = 2,
        March = 3,
        April = 4,
        May = 5,
        Jun = 6,
        July = 7,
        August = 8,
        September = 9,
        Oktober = 10,
        November = 11,
        December = 12
    }
    public class MonthShortInfo
    {
        public double TopAmount { get; set; }
        public double BottomAmount { get; set; }
        public double Spent { get; set; }
        public Month Month { get; set; }
        public int Year { get; set; }
        public DateTime StartDate { get; set; }
    }
}
