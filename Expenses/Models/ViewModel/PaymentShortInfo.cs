﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Threading.Tasks;

namespace Expenses.Models.ViewModel
{
    /// <summary>
    /// Short info for displaying on main page.
    /// </summary>
    public class PaymentShortInfo
    {
        public Color GroupColor { get; set; } 
        public string Group { get; set; }
        public string Shop { get; set; }
        public DateTime Date { get; set; }
        public double Amount { get; set; }
    }
}
