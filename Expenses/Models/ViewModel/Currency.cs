﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Expenses.Models.ViewModel
{
    public interface ICurrency
    {
        string Sign { get; }
        string Name { get; }
        string ConvertToString(double amount);
    }
    public class Dinar : ICurrency
    {
        public string Sign => "din";
        public string Name => "Dinar";
        public string ConvertToString(double amount)
            => amount.ToString("N") + " " + Sign;
        
    }
}
