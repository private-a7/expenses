﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Expenses.Models.ViewModel
{
    public class PaymentItemViewModel
    {
        public string Name { get; set; }
        public double Amount { get; set; }
    }
    public class PaymentViewModel
    {
        [Required(AllowEmptyStrings =false)]
        public string Group { get; set; }

        [Required(AllowEmptyStrings =false)]
        public string Shop { get; set; }

        [Required(AllowEmptyStrings =false)]
        public string PaymentMethod { get; set; }

        public IEnumerable<string> Tags { get; set; }
        public DateTime Date { get; set; }


        [Required]
        public IEnumerable<PaymentItemViewModel> Items { get; set; }

        public string Comment { get; set; }
    }
}
