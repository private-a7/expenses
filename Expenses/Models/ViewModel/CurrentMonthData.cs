﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Expenses.Models.ViewModel
{
    public class CurrentMonthData
    {
        public double Spent { get; set; }
        public double TopLimit { get; set; }
        public double BottomLimit { get; set; }

        public DateTime StartDate { get; set; }
        public ICurrency Currency { get; set; }

        public IEnumerable<PaymentShortInfo> Payments { get; set; }
        public IEnumerable<GroupShortInfo> Groups { get; set; }
        public IEnumerable<MonthShortInfo> Months { get; set; }
    }
}
