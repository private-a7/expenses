﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Expenses.Models.ViewModel
{
    public class LoginModel
    {
        [Required()]
        public string Name { get; set; }

        [Required(AllowEmptyStrings = false)]
        [MinLength(6)]
        public string Password { get; set; }

    }
}
