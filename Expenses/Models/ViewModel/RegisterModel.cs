﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Expenses.Models.ViewModel
{
    public class RegisterModel
    {
        public string Username { get; set; }

        [Required()]
        [EmailAddress(ErrorMessage = "Email format is invalid!")]
        public string Email { get; set; }
        [Required(AllowEmptyStrings = false)]
        [MinLength(6)]
        public string Password { get; set; }

        [Required(AllowEmptyStrings = false)]
        [MinLength(6)]
        public string Password1 { get; set; }

    }
}
