﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Expenses
{
    public static class DateTimeExtensions
    {
        public static string ToJSON(this DateTime dt)
        {
            return dt.ToString("yyyy-MM-ddTHH:MM:ss.000Z");
        }
    }
}
