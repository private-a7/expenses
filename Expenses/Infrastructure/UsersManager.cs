﻿using Expenses.Model;
using Expenses.Models.Data;
using Expenses.Repository;
using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Expenses.Infrastructure
{
    public class UsersManager
    {
        private IUserRepository user { get; set; }
        private UserManager<AppUser> appUser { get; set; }

        public UsersManager(IUserRepository user, UserManager<AppUser> appUser)
        {
            this.user = user;
            this.appUser = appUser;
        }
        public async Task<Tuple<AppUser, User>> GetAppUserAsync(ClaimsPrincipal claims)
        {
            var appU =  await appUser.GetUserAsync(claims).ConfigureAwait(false);
            var u = user.GetUser(appU.Email);
            return new Tuple<AppUser, User>(appU, u);
        }
    }
}
