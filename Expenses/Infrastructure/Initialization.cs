﻿using Expenses.Models.Data;
using Expenses.MySQLRepository;
using Expenses.Repository;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Expenses.Infrastructure
{
    public class Initialization
    {
        public static void Seed(IServiceProvider provider, IConfiguration config)
        {
            using (var scope = provider.CreateScope())
            {
                RoleManager<IdentityRole> roleManager = scope.ServiceProvider.GetRequiredService<RoleManager<IdentityRole>>();
                UserManager<AppUser> userManager = scope.ServiceProvider.GetRequiredService<UserManager<AppUser>>();
                var users = scope.ServiceProvider.GetRequiredService<IUserRepository>();


                SeedRoles(userManager, roleManager);
                SeedUsers(userManager, config, users);
            }
        }
        private static void SeedRoles(UserManager<AppUser> userMgr, RoleManager<IdentityRole> roleMgr)
        {
            foreach (string s in RoleNames.AllRoles)
            {
                if (roleMgr.FindByNameAsync(s).Result == null)
                {
                    var res = roleMgr.CreateAsync(new IdentityRole(s));
                    if (!res.Result.Succeeded)
                        throw new ApplicationException("Unable to create role!");
                }
            }

        }
        private static async Task SeedUsers(UserManager<AppUser> userMgr, IConfiguration config, IUserRepository usersRepo)
        {
            var section = config.GetSection("DefaultUsers");
            foreach (var child in section.GetChildren())
            {
                string user = child["Name"];
                string pass = child["Password"];
                bool isAdmin = child["IsAdmin"] == "true";
                var appUser = await userMgr.FindByEmailAsync(user);
                if (appUser == null)
                {
                    appUser = new AppUser()
                    {
                        UserName = user,
                        Email = user
                    };
                    var res1 = userMgr.CreateAsync(appUser, pass);
                    if (res1.Result != null && !res1.Result.Succeeded)
                    {
                        throw new InvalidOperationException("Default user not created!\n" + res1.Result.Errors.First().Description);
                    }
                    var res2 = userMgr.AddToRoleAsync(appUser, RoleNames.Admin);
                }
                var u = usersRepo.GetUser(user);
                if (u == null)
                {
                    usersRepo.Save(new Model.User() { Email = user });
                }

            }
        }
    }
}
