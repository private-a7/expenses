﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Expenses.Infrastructure
{
    public class RoleNames
    {
        public const string Admin = "Admin";
        public const string AllUsers = "Users";
        public static IEnumerable<string> AllRoles => roles;

        private static List<string> roles = new List<string>() { Admin, AllUsers };

        public static void AddRole(string role) => roles.Add(role);
        public static void RemoveRole(string role) => roles.Remove(role);

        public static void ClearRoles()
        {
            roles.Clear();
            roles.Add(Admin);
            roles.Add(AllUsers);
        }
    }
}
