﻿using Expenses.Model;
using Expenses.Models.Data;
using Expenses.Repository;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Expenses.Component
{
    public class Sidebar:ViewComponent
    {

        private UserManager<AppUser> userMgr;
        private IUserRepository userRepository;

        public Sidebar(UserManager<AppUser> userMgr, IUserRepository userRepository)
        {
            this.userMgr = userMgr;
            this.userRepository = userRepository;
        }

        public async Task<IViewComponentResult> InvokeAsync()
        {
            bool isAdmin = false;
            string name = "";
            AppUser appUser = await userMgr.GetUserAsync(this.HttpContext.User);
            if (appUser != null)
            {
                User user = userRepository.GetUser(appUser.Email);
                if (user != null)
                {
                    isAdmin = false;
                    name = user.Email;
                }
            }
            ViewBag.IsAdmin = isAdmin;
            ViewBag.FullName = name;
            return View();
        }
    }
}
