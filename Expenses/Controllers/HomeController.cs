﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Expenses.Models;
using Microsoft.AspNetCore.Identity;
using Expenses.Models.Data;
using Microsoft.AspNetCore.Authorization;
using Expenses.Repository;
using Expenses.Model;
using Expenses.Models.ViewModel;
using Expenses.Infrastructure;

namespace Expenses.Controllers
{
    public class HomeController : Controller
    {
        private ILogger<HomeController> _logger;
        private UserManager<AppUser> userMgr;
        private SignInManager<AppUser> signMgr;
        private IUserRepository userRepository;

        public HomeController(ILogger<HomeController> logger, UserManager<AppUser> userMgr, SignInManager<AppUser> signMgr, IUserRepository userRepository)
        {
            _logger = logger;
            this.userMgr = userMgr;
            this.signMgr = signMgr;
            this.userRepository = userRepository;
        }

        public async Task<IActionResult> Index()
        {
            AppUser current = await userMgr.GetUserAsync(HttpContext.User).ConfigureAwait(false);
            if (User.Identity.IsAuthenticated)
            {
                RedirectToAction(nameof(AccountController.Index), nameof(AccountController));
            }
            return RedirectToAction(nameof(Login));
            //return View();
        }

        public IActionResult Login(string returnUrl = null)
        {
            // If already logged in, skip.
            ViewBag.ReturnUrl = returnUrl;
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> Login(LoginModel details, string returnUrl = null)
        {
            ViewBag.ReturnUrl = returnUrl;
            await signMgr.SignOutAsync();
            if (ModelState.IsValid)
            {
                bool valid = true;
                AppUser appUser = null;
                appUser = await userMgr.FindByNameAsync(details.Name).ConfigureAwait(false);
                if (appUser == null)
                    appUser = await userMgr.FindByEmailAsync(details.Name).ConfigureAwait(false);
                if (appUser == null)
                {
                    ModelState.AddModelError(nameof(LoginModel.Name), "Username or Email not found!");
                    valid = false;
                }
                if (valid)
                {
                    if (!await userMgr.CheckPasswordAsync(appUser, details.Password))
                    {
                        ModelState.AddModelError("Password","Invalid password!");
                        valid = false;
                    }
                }
                if (valid)
                {
                    await signMgr.SignOutAsync();
                    await signMgr.SignInAsync(appUser, false);
                    if (returnUrl != null)
                        return Redirect(returnUrl);
                    else
                        return RedirectToAction(nameof(AccountController.Index), "Account");

                }
            }
            return View(details);
        }

        public IActionResult Register()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Register(RegisterModel details)
        {
            if (ModelState.IsValid)
            {
                bool valid = true;
                AppUser appUser = null;
                if (!string.IsNullOrEmpty(details.Username))
                {
                    appUser = await userMgr.FindByNameAsync(details.Username).ConfigureAwait(false);
                    if (appUser != null)
                    {
                        ModelState.AddModelError(nameof(RegisterModel.Username), "Username already exists!");
                        valid = false;
                    }
                }
                appUser = await userMgr.FindByEmailAsync(details.Email).ConfigureAwait(false);
                if (appUser != null)
                {
                    ModelState.AddModelError(nameof(RegisterModel.Email), "Email already exists!");
                    valid = false;
                }
                if (valid && details.Password != details.Password1)
                {
                    ModelState.AddModelError(nameof(RegisterModel.Password), "Passwords do not match!");
                    ModelState.AddModelError(nameof(RegisterModel.Password1), "Passwords do not match!");
                    valid = false;
                }
                if (valid)
                {
                    string username = string.IsNullOrEmpty(details.Username) ? details.Email : details.Username;
                    appUser = new AppUser() { UserName = username, Email = details.Email };
                    IdentityResult res = await userMgr.CreateAsync(appUser, details.Password).ConfigureAwait(false);
                    if (res.Succeeded)
                    {
                        await userMgr.AddToRoleAsync(appUser, RoleNames.AllUsers).ConfigureAwait(false);
                        User user = new User() { Email = details.Email };
                        user.PaymentGroup = new List<PaymentGroup>() { 
                            new PaymentGroup() 
                            { 
                                Name = "[Default]", 
                                User = user, 
                                Color = 0x0000ff ,
                                Shops = new List<Shop>()
                            } 
                        };
                        user.PaymentGroup.First().Shops.Add(
                            new Shop()
                            {
                                Name = "radnja",
                                Group = user.PaymentGroup.First()
                            });
                        user.Methods = new List<PaymentMethod>() { new PaymentMethod() { Method = "cash" }, new PaymentMethod() { Method = "card" } };
                        userRepository.Save(user);
                        LoginModel login = new LoginModel() { Name = details.Email };
                        return RedirectToAction("Login", login);
                    }
                    foreach (IdentityError error in res.Errors)
                        ModelState.AddModelError("", $"{error.Code} : {error.Description}");
                }
            }
            return View(details);
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Logout()
        {
            await signMgr.SignOutAsync();
            return RedirectToAction(nameof(Login));
        } 

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

    }
}
