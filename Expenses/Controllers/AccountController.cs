﻿using Expenses.Infrastructure;
using Expenses.Model;
using Expenses.Models;
using Expenses.Models.Data;
using Expenses.Models.ViewModel;
using Expenses.MySQLRepository;
using Expenses.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace Expenses.Controllers
{
    [Authorize]
    public class AccountController : Controller
    {
        private ILogger<HomeController> _logger;
        private UserManager<AppUser> userMgr;
        private SignInManager<AppUser> signMgr;
        private IUserRepository userRepository;
        private IGroupRepository groupRepository;
        private IPaymentRepository paymentRepository;

        public AccountController(ILogger<HomeController> logger, UserManager<AppUser> userMgr,
            SignInManager<AppUser> signMgr, IUserRepository userRepository,
            IGroupRepository groupRepository, IPaymentRepository paymentRepository)
        {
            _logger = logger;
            this.userMgr = userMgr;
            this.signMgr = signMgr;
            this.userRepository = userRepository;
            this.groupRepository = groupRepository;
            this.paymentRepository = paymentRepository;
        }

        public async Task<IActionResult> AddPayment(string result = null)
        {
            AppUser appUser = await userMgr.GetUserAsync(this.HttpContext.User);
            if (appUser == null)
                return NotFound("User not logged in!");
            User user = userRepository.GetUser(appUser.Email);
            if (user == null)
                return NotFound("User not found!");
            if (result == "success")
                ViewBag.PaymentSuccess = true;
            setPaymentViewBag(user);
            return View();
        }
        [HttpPost]
        public async Task<IActionResult> AddPayment([FromForm] PaymentViewModel p)
        {
            string s = Newtonsoft.Json.JsonConvert.SerializeObject(DateTime.Now);
            AppUser appUser = await userMgr.GetUserAsync(this.HttpContext.User);
            if (appUser == null)
                return NotFound("User not logged in!");
            User user = userRepository.GetUser(appUser.Email);
            if (user == null)
                return NotFound("User not found!");
            ViewBag.Action = nameof(Index);
            var groups = groupRepository.GetAll(x => x.UserId == user.Id);

            if (ModelState.IsValid)
            {
                // If group doesn't exist, create it.
                PaymentGroup group = groupRepository.GetGroup(user.Id, p.Group);

                Shop shop = group != null ? group.Shops.FirstOrDefault(x => x.Name == p.Shop) : null;
                if (group == null)
                {
                    group = new PaymentGroup()
                    {
                        Name = p.Group,
                        Shops = new List<Shop>()
                    };
                    group.User = user;
                }
                if (shop == null)
                {
                    shop = new Shop()
                    {
                        Group = group,
                        Name = p.Shop
                    };
                    group.Shops.Add(shop);
                }
                groupRepository.Save(group);

                Payment payment = new Payment()
                {
                    Items = p.Items.Select(x => new PaymentItem() { Amount = x.Amount, Description = x.Name }).ToList(),
                    PaymentComment = !string.IsNullOrEmpty(p.Comment) ? new PaymentComment() { Comment = p.Comment } : null,
                    TotalAmount = p.Items.Sum(x => x.Amount),
                    PurchaseDate = p.Date,
                    Tags = p.Tags?.Select(x => new PaymentTag() { Tag = x }).ToList() ?? null,
                    UserId = user.Id,
                    PaymentMethod = new PaymentMethod() { Method = p.PaymentMethod },
                    Shop = shop
                };

                // Save file
                userRepository.Save(user);
                paymentRepository.Save(payment);

                if (!ModelState.IsValid)
                {
                    setPaymentViewBag(user);
                    return View(p);
                }
                return RedirectToAction(nameof(AddPayment), new { result = "success" });// _AddPayment();
            }
            return View(p);
        }
        private void setPaymentViewBag(User user)
        {
            ViewBag.Groups = user.PaymentGroup.Select(x => x.Name).ToArray();// new List<string>() {"[Default]", "Group 1", "Group 2" };
            ViewBag.Shops = user.PaymentGroup.SelectMany(x => x.Shops).Select(x => x.Name).Distinct().ToArray();
            ViewBag.Tags = user.Tags?.Select(x => x.Tag).ToArray() ?? new string[] { }; //new List<string>() { "Tag 1", "Tag 2", "Tag 3" };
            ViewBag.PaymentMethods = user.Methods.Select(x => x.Method);// new List<string>() { "Method 1", "Method 2" };
        }
        public async Task<IActionResult> Index()
        {
            AppUser appUser = await userMgr.GetUserAsync(this.HttpContext.User);
            if (appUser == null)
                return NotFound("User not logged in!");
            User user = userRepository.GetUser(appUser.Email);
            if (user == null)
                return NotFound("User not found!");
            ViewBag.Action = nameof(Index);
            var groups = groupRepository.GetAll(x => x.UserId == user.Id);

            int startDay = user.FirstDayInMonth;
            DateTime startDate = MonthCalculator.GetStartPeriod(startDay, DateTime.Now);
            DateTime endDate = MonthCalculator.GetEndPeriod(startDay, startDate);

            int numberOfLastMonths = 3;

            IEnumerable<Payment> allPayments = paymentRepository.GetAllForUser(user.Id, startDate.AddMonths(-numberOfLastMonths + 1));
            IEnumerable<Payment>[] payments = new IEnumerable<Payment>[numberOfLastMonths];
            Limit[] limits = new Limit[numberOfLastMonths];
            Tuple<DateTime, DateTime>[] dateRanges = new Tuple<DateTime, DateTime>[numberOfLastMonths];
            for (int i = 0; i < numberOfLastMonths; i++)
            {
                DateTime dateS = startDate.AddMonths(-i);
                DateTime dateE = MonthCalculator.GetEndPeriod(startDay, dateS);
                dateRanges[i] = new Tuple<DateTime, DateTime>(dateS, dateE);
                var p = allPayments.Where(x => dateS <= x.PurchaseDate && x.PurchaseDate <= dateE).ToList();
                payments[i] = p;
                Limit l = user.GetLimit(dateS, dateE);
                if (l != null)
                    limits[i] = l;
                //if (i == 0 && l == null)
                    //limits[i] = user.GetPreviousLimit(dateS);
            }
            for (int i = numberOfLastMonths - 1; i >= 0; i--)
            {
                if (limits[i] != null)
                    continue;
                if (i + 1 == numberOfLastMonths)
                {
                    limits[i] = user.GetPreviousLimit(dateRanges[i].Item1);
                    if (limits[i] == null)
                        limits[i] = new Limit() { StartDate = dateRanges[i].Item1, TopAmount = 0, BottomAmount = 0 };
                }
                else
                {
                    var last = limits[i + 1];
                    limits[i] = new Limit() { StartDate = dateRanges[i].Item1, BottomAmount = last.BottomAmount, TopAmount = last.TopAmount };
                }
            }

            Dictionary<int, PaymentGroup> shopGroup = user.PaymentGroup.SelectMany(x => x.Shops).ToDictionary(x => x.Id, y => y.Group);

            CurrentMonthData data = new CurrentMonthData()
            {
                BottomLimit = limits[0].BottomAmount,
                TopLimit = limits[0].TopAmount,
                Spent = payments[0].Sum(x => x.TotalAmount),
                StartDate = startDate,// DateTime.Now.Subtract(new TimeSpan(2, 0, 0, 0)),
                Currency = new Dinar()
            };
            data.Payments = payments[0].Select(x => new PaymentShortInfo()
            {
                GroupColor = Color.FromArgb(shopGroup[x.ShopId.Value].Color),
                Group = x.Shop.Group.Name,
                Shop = x.Shop.Name,
                Amount = x.TotalAmount,
                Date = x.PurchaseDate
            }).ToList();

            data.Groups = payments[0].GroupBy(x => x.Shop.Group.Name).Select(x => new GroupShortInfo()
            {
                Name = x.Key,// shopGroup[x.ShopId.Value].Name,
                Color = Color.FromArgb(shopGroup.Where(y => y.Value.Name == x.Key).FirstOrDefault().Value.Color),
                Amount = x.Sum(y => y.TotalAmount)
            }).ToList();
            var months = new List<MonthShortInfo>();
            for (int i = numberOfLastMonths - 1; i >= 0; i--)
            {
                var m = new MonthShortInfo();
                m.Month = (Month)MonthCalculator.GetMonth(startDay, dateRanges[i].Item1);
                m.Year = MonthCalculator.GetYear(startDay, dateRanges[i].Item1);
                m.TopAmount = limits[i].TopAmount;
                m.BottomAmount = limits[i].BottomAmount;
                m.StartDate = dateRanges[i].Item1;
                m.Spent = payments[i].Sum(x => x.TotalAmount);
                months.Add(m);
            }
            data.Months = months;

            ViewBag.StartDate = startDate;
            ViewBag.EndDate = endDate;
            setPaymentViewBag(user);
            return View(data);
        }
        public IActionResult Monthly()
        {
            ViewBag.Action = nameof(Monthly);

            return View();
        }

        [HttpGet()]
        public async Task<IActionResult> Payments(string  start, string end)
        {
            AppUser appUser = await userMgr.GetUserAsync(this.HttpContext.User);
            if (appUser == null)
                return NotFound("User not logged in!");
            User user = userRepository.GetUser(appUser.Email);
            if (user == null)
                return NotFound("User not found!");
            ViewBag.Action = nameof(Index);
            int startDay = user.FirstDayInMonth;
            DateTime startDate = DateTime.Now;
            DateTime endDate = DateTime.Now;
            string startS = $"\"{start}-01T01:00:00\"";
            string endS = $"\"{end}-01T01:00:00\"";
            try
            {
                startDate = JsonSerializer.Deserialize<DateTime>(startS);
            }
            catch
            {
                startDate = DateTime.Now;
            }
            try
            {
                endDate = JsonSerializer.Deserialize<DateTime>(endS);
            }
            catch
            {
                endDate = DateTime.Now;
            }
            DateTime s = startDate;// start ?? DateTime.Now;
            DateTime e = endDate;// end ?? DateTime.Now;
            startDate = MonthCalculator.GetStartPeriod(startDay, s.Year, s.Month);
            endDate = MonthCalculator.GetEndPeriod(startDay, e.Year, e.Month);

            ViewBag.Action = nameof(Payments);
            var groups = groupRepository.GetAll(x => x.UserId == user.Id);

            var payments = paymentRepository.GetAllForUser(user.Id, startDate, endDate);
            
            ViewBag.Groups = groups;
            ViewBag.StartMonth =MonthCalculator.GetMonth(startDay, startDate);
            ViewBag.EndMonth = MonthCalculator.GetMonth(startDay, endDate);
            ViewBag.StartYear = MonthCalculator.GetYear(startDay, startDate);
            ViewBag.EndYear = MonthCalculator.GetYear(startDay, endDate);
            ViewBag.StartDate = startDate;
            ViewBag.EndDate = endDate;

            FilteredDataModel model = new FilteredDataModel();
            model.StartDay = startDay;
            model.Payments = payments.OrderByDescending(x=>x.PurchaseDate).ToList();
            model.StartDate = startDate;
            model.EndDate = endDate;

            return View(model);
        }
        [HttpGet()]
        public async Task<IActionResult> EditPayment(int id)
        {
            AppUser appUser = await userMgr.GetUserAsync(this.HttpContext.User);
            if (appUser == null)
                return NotFound("User not logged in!");
            User user = userRepository.GetUser(appUser.Email);
            if (user == null)
                return NotFound("User not found!");
            ViewBag.Action = nameof(Index);
            var groups = groupRepository.GetAll(x => x.UserId == user.Id);

            Payment p = paymentRepository.Get(id);
            if (p == null)
            {
                ModelState.AddModelError("", "Payment doesn't exist");
                return NotFound($"Payment with id {id} was not found!");
            }

            PaymentViewModel model = new PaymentViewModel()
            {
                Shop = p.Shop.Name,
                Group = p.Shop.Group.Name,
                Date = p.PurchaseDate,
                Comment = p.PaymentComment?.Comment,
                Items = p.Items.Select(x => new PaymentItemViewModel() { Amount = x.Amount, Name = x.Description }).ToList(),
                PaymentMethod = p.PaymentMethod.Method,
                Tags = p.Tags.Select(x => x.Tag).ToList()
            };
            ViewBag.Id = id;
            setPaymentViewBag(user);
            return View(model);
        }
        [HttpPost]
        public async Task<IActionResult> EditPayment(int id, [FromForm] PaymentViewModel p)
        {
            string s = Newtonsoft.Json.JsonConvert.SerializeObject(DateTime.Now);
            AppUser appUser = await userMgr.GetUserAsync(this.HttpContext.User);
            if (appUser == null)
                return NotFound("User not logged in!");
            User user = userRepository.GetUser(appUser.Email);
            if (user == null)
                return NotFound("User not found!");
            ViewBag.Action = nameof(Index);
            var groups = groupRepository.GetAll(x => x.UserId == user.Id);

            if (ModelState.IsValid)
            {
                Payment py = paymentRepository.Get(id);
                if (py == null)
                {
                    ModelState.AddModelError("", "Payment doesn't exist");
                    return NotFound(p);
                }
                if (py.Shop.Group.Name != p.Group)
                {
                    var group = new PaymentGroup()
                    {
                        Name = p.Group,
                        User = user,
                        Shops = new List<Shop>()
                    };
                    Shop sh = new Shop() { Group = group, Name = p.Shop };
                    group.Shops.Add(sh);
                    groupRepository.Save(group);
                    py.Shop = sh;
                }
                if (py.Shop.Name != p.Shop)
                {
                    Shop shop = new Shop() { Group = py.Shop.Group, Name = p.Shop };
                    py.Shop.Group.Shops.Add(shop);
                    py.Shop = shop;
                    groupRepository.Save(py.Shop.Group);
                }
                if (py.PaymentComment?.Comment != p.Comment)
                {
                    if (py.PaymentComment != null)
                        paymentRepository.DeleteComment(py.PaymentComment.Id);
                    PaymentComment comm = new PaymentComment() { Comment = p.Comment };
                    var old = py.PaymentComment;
                    py.PaymentComment = comm;
                }
                py.Items?.Clear();
                py.Items = p.Items.Select(x => new PaymentItem() { Amount = x.Amount, Description = x.Name }).ToList();
                py.TotalAmount = p.Items.Sum(x => x.Amount);
                py.PurchaseDate = p.Date;
                paymentRepository.Save(py);

                return Ok("{}");
            }
            return this.BadRequest();
        }
        public async Task<IActionResult> Settings()
        {

            string s = Newtonsoft.Json.JsonConvert.SerializeObject(DateTime.Now);
            AppUser appUser = await userMgr.GetUserAsync(this.HttpContext.User);
            if (appUser == null)
                return NotFound("User not logged in!");
            User user = userRepository.GetUser(appUser.Email);
            if (user == null)
                return NotFound("User not found!");
            ViewBag.Action = nameof(Index);
            var groups = groupRepository.GetAll(x => x.UserId == user.Id);

            ViewBag.Limits = user.Limit;
            ViewBag.StartDateJson= MonthCalculator.GetStartPeriod(user.FirstDayInMonth, new DateTime(2020, 1, user.FirstDayInMonth).AddDays(-1)).ToJSON();
            ViewBag.Groups = groups;
            ViewBag.Action = nameof(Settings);
            return View();
        }

        public IActionResult Preferences()
        {
            ViewBag.Action = nameof(Preferences);
            return View();
        }

        public IActionResult Account()
        {
            ViewBag.Action = nameof(Account);
            return View();
        }

        [Authorize(Roles = RoleNames.Admin)]
        [Route("[controller]/Admin/Settings")]
        public IActionResult AdminSettings()
        {
            ViewBag.Action = nameof(AdminSettings);
            return View();
        }
    }
}
