﻿using Expenses.Model;
using Expenses.Models;
using Expenses.Models.Data;
using Expenses.Models.ImportExports;
using Expenses.Models.ViewModel;
using Expenses.MySQLRepository;
using Expenses.Repository;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Expenses.Controllers
{
    //[Route("api/[controller]")]
    [Authorize]
    public class ApiController : Controller
    {

        private ILogger<HomeController> _logger;
        private UserManager<AppUser> userMgr;
        private SignInManager<AppUser> signMgr;
        private IUserRepository userRepository;
        private IGroupRepository groupRepository;
        private IPaymentRepository paymentRepository;

        public ApiController(ILogger<HomeController> logger, UserManager<AppUser> userMgr,
            SignInManager<AppUser> signMgr, IUserRepository userRepository,
            IGroupRepository groupRepository, IPaymentRepository paymentRepository)
        {
            _logger = logger;
            this.userMgr = userMgr;
            this.signMgr = signMgr;
            this.userRepository = userRepository;
            this.groupRepository = groupRepository;
            this.paymentRepository = paymentRepository;
        }
        [HttpGet]
        public IActionResult prvi()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> GetMonthLimits([FromBody] MonthRangeModel range)
        {
            // Get from date
            // Get to date
            AppUser appUser = await userMgr.GetUserAsync(this.HttpContext.User);
            if (appUser == null)
                return NotFound(new { Error = "User not logged in!" });
            User user = userRepository.GetUser(appUser.Email);
            if (user == null)
                return NotFound(new { Error = "User not found!" });

            var groups = groupRepository.GetGroups(user.Id);

            if (ModelState.IsValid)
            {
                int startDay = user.FirstDayInMonth;
                if (startDay == 0)
                    startDay = 1;
                // Set start and end date so it first FirstDayInMonth.
                //var d = range.StartDate;
                int startYear = MonthCalculator.GetYear(startDay, range.StartDate);
                int startMonth = MonthCalculator.GetMonth(startDay, range.StartDate);
                int endYear = MonthCalculator.GetYear(startDay, range.EndDate);
                int entMonth = MonthCalculator.GetMonth(startDay, range.EndDate);
                DateTime startD = MonthCalculator.GetStartPeriod(startDay, startYear, startMonth);// new DateTime(d.Year, d.Month, 1, 0, 0, 0);
                //d = range.EndDate;
                DateTime endD = MonthCalculator.GetEndPeriod(startDay, endYear, entMonth);
                //if (startDay > 15)
                //    startD = startD.AddMonths(-1);
                //else
                //    endD = endD.AddMonths(1);
                //startD = startD.AddDays(startDay - 1);
                //endD = endD.AddDays(startDay - 1);

                var payments = paymentRepository.GetAllForUser(user.Id, startD, endD)
                    //.GetAll((p) => startD <= p.PurchaseDate && p.PurchaseDate <= endD)
                    .OrderBy(x => x.PurchaseDate)
                    .ToList();

                // Prepare date ranges.
                var paymentsDic = new[] { new { Date = DateTime.Now, Limit = new List<Limit>(), Payments = new List<Payment>() } }.ToList();
                paymentsDic.Clear();
                DateTime tempDate = startD;
                while (tempDate < endD)
                {
                    paymentsDic.Add(new { Date = tempDate, Limit = new List<Limit>(), Payments = new List<Payment>() });
                    tempDate = tempDate.AddMonths(1);
                }
                // Assign existing limits. 
                if (user.Limit != null)
                    for (int i = 0; i < user.Limit.Count; i++)
                    {
                        Limit lm = user.Limit.ElementAt(i);
                        DateTime lmStart = MonthCalculator.GetStartPeriod(startDay, lm.StartDate);
                        if (lmStart < startD || endD < lmStart)
                            continue;
                        for (int j = 0; j < paymentsDic.Count; j++)
                        {
                            DateTime dt = paymentsDic[j].Date;
                            if (lmStart <= dt && dt <= lmStart.AddMonths(1))
                            {
                                paymentsDic[j].Limit.Add(lm);
                                break;
                            }
                        }
                    }
                // Initial limit is not in date range. Try to find previous one.
                if (!paymentsDic[0].Limit.Any())
                {
                    var pl = user.GetPreviousLimit(startD);
                    Limit l = new Limit() { TopAmount = 0, BottomAmount = 0, StartDate = startD };
                    if (pl != null)
                    {
                        l.TopAmount = pl.TopAmount;
                        l.BottomAmount = pl.BottomAmount;
                    }
                    paymentsDic[0].Limit.Add(l);
                }

                // Where limits are not set, use previous one.
                for (int i = 0; i < paymentsDic.Count; i++)
                {
                    var v = paymentsDic[i];
                    if (!v.Limit.Any())
                    {
                        if (i == 0)
                        {
                            // This should have been handled before.
                            //Limit l = new Limit() { TopAmount = 0, BottomAmount = 0, StartDate = v.Date };
                            //v.Limit.Add(l);
                        }
                        else
                        {
                            var previous = paymentsDic[i - 1].Limit.First();
                            v.Limit.Add(new Limit()
                            {
                                StartDate = previous.StartDate.AddMonths(1),
                                TopAmount = previous.TopAmount,
                                BottomAmount = previous.BottomAmount
                            });
                        }
                    }
                }

                // Assign payments and limits to date ranges.
                for (int i = 0; i < payments.Count; i++)
                {
                    var p = payments[i];
                    int j = 0;
                    while (paymentsDic[j].Date.AddMonths(1) < p.PurchaseDate)
                        j++;
                    if (j > paymentsDic.Count)
                        throw new InvalidOperationException("Payment is out of date range!");
                    paymentsDic[j].Payments.Add(p);
                }
                List<MonthLimitsModel> months = new List<MonthLimitsModel>();
                // Calculate payments in each month.
                for (int i = 0; i < paymentsDic.Count; i++)
                {
                    var p = paymentsDic[i];
                    var m = new MonthLimitsModel();
                    m.TopLimit = p.Limit.First().TopAmount;
                    m.BottomLimit = p.Limit.First().BottomAmount;
                    m.Spent = p.Payments.Sum(x => x.TotalAmount);
                    m.StartDate = p.Date;
                    m.Month = MonthCalculator.GetMonth(startDay, p.Date);
                    m.Year = MonthCalculator.GetYear(startDay, p.Date);
                    m.EndDate = p.Date.AddMonths(1);
                    months.Add(m);
                }
                MonthsModel model = new MonthsModel();
                model.StartDate = startD;
                model.EndDate = endD;
                model.Months = months;

                // create collection
                return new JsonResult(model);
            }
            return BadRequest("Invalid data is sent");
        }

        [HttpPost]
        public IActionResult ChangeGroup([FromBody] GroupUpdateModel data)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Data is invalid!");
            }
            AppUser appUser = userMgr.GetUserAsync(this.HttpContext.User).Result;
            if (appUser == null)
                return NotFound(new { Error = "User not logged in!" });
            User user = userRepository.GetUser(appUser.Email);
            if (user == null)
                return NotFound(new { Error = "User not found!" });

            var groups = groupRepository.GetGroups(user.Id);

            var gr = groups.FirstOrDefault(x => x.Id == data.Id);
            if (gr == null)
                return BadRequest("Group not found.");

            gr.Name = data.Name;
            gr.Color = data.Color;

            groupRepository.Save(gr);

            return Ok();
        }
        [HttpPost]
        public IActionResult ChangeStartDay([FromBody]int day)
        {
            AppUser appUser = userMgr.GetUserAsync(this.HttpContext.User).Result;
            if (appUser == null)
                return NotFound(new { Error = "User not logged in!" });
            User user = userRepository.GetUser(appUser.Email);
            if (user == null)
                return NotFound(new { Error = "User not found!" });

            user.FirstDayInMonth = day;
            userRepository.Save(user);

            return Ok();
        }
        [HttpPost()]
        public IActionResult ChangeShopName([FromBody] ShopUpdateModel data)
        {
            if (!ModelState.IsValid)
                return BadRequest("Post data is invalid");
            AppUser appUser = userMgr.GetUserAsync(this.HttpContext.User).Result;
            if (appUser == null)
                return NotFound(new { Error = "User not logged in!" });
            User user = userRepository.GetUser(appUser.Email);
            if (user == null)
                return NotFound(new { Error = "User not found!" });

            var gr = user.PaymentGroup.FirstOrDefault(x => x.Id == data.GroupId);
            if (gr == null)
                return BadRequest($"Group id {data.GroupId} is invalid.");
            var sh = gr.Shops.FirstOrDefault(x => x.Id == data.Id);
            if (sh == null)
                return BadRequest($"Shop id {data.Id} is invalid.");

            sh.Name = data.Name;
            userRepository.Save(user);

            return Ok();
        }

        [HttpPost]
        public IActionResult AddLimit()
        {
            AppUser appUser = userMgr.GetUserAsync(this.HttpContext.User).Result;
            if (appUser == null)
                return NotFound(new { Error = "User not logged in!" });
            User user = userRepository.GetUser(appUser.Email);
            if (user == null)
                return NotFound(new { Error = "User not found!" });
            var date = DateTime.Now;
            int startDay = user.FirstDayInMonth;
            date = new DateTime(MonthCalculator.GetYear(startDay, date), MonthCalculator.GetMonth(startDay, date), 1);
            user.Limit.Add(new Limit() { StartDate = new DateTime(date.Year, date.Month, 1)});
            userRepository.Save(user);

            return Ok();
        }
        [HttpPost]
        public IActionResult UpdateLimit([FromBody] LimitModel limit)
        {

            AppUser appUser = userMgr.GetUserAsync(this.HttpContext.User).Result;
            if (appUser == null)
                return NotFound(new { Error = "User not logged in!" });
            User user = userRepository.GetUser(appUser.Email);
            if (user == null)
                return NotFound(new { Error = "User not found!" });

            if (!ModelState.IsValid)
                return this.BadRequest();
            Limit l = user.Limit.Where(x => x.Id == limit.Id).FirstOrDefault();
            if (l == null)
                return NotFound("Not found limit " + limit.Id);

            l.TopAmount = limit.TopLimit;
            l.BottomAmount = limit.BottomLimit;
            l.StartDate = limit.Date;
            userRepository.Save(user);

            return Ok("{}");
        }

        [HttpPost]
        public IActionResult DeleteLimit(int id)
        {
            AppUser appUser = userMgr.GetUserAsync(this.HttpContext.User).Result;
            if (appUser == null)
                return NotFound(new { Error = "User not logged in!" });
            User user = userRepository.GetUser(appUser.Email);
            if (user == null)
                return NotFound(new { Error = "User not found!" });

            Limit l = user.Limit.FirstOrDefault(x => x.Id == id);
            if (l != null)
            {
                user.Limit.Remove(l);
                userRepository.Save(user);
            }
            else 
                return Ok("id not found");
            return Ok("ok");
        }

        [HttpPost]
        public IActionResult ExportAll()
        {
            AppUser appUser = userMgr.GetUserAsync(this.HttpContext.User).Result;
            if (appUser == null)
                return NotFound(new { Error = "User not logged in!" });
            User user = userRepository.GetUser(appUser.Email);
            if (user == null)
                return NotFound(new { Error = "User not found!" });

            string s = Export.ToJson(paymentRepository, user);
            byte[] bytes = UTF8Encoding.UTF8.GetBytes(s);
            return File(bytes, "application/json", "export.json");
        }
    }
}

