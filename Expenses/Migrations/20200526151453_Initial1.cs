﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Expenses.Migrations
{
    public partial class Initial1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Payments_PaymentComments_PaymentCommentId",
                table: "Payments");

            migrationBuilder.AlterColumn<int>(
                name: "PaymentCommentId",
                table: "Payments",
                nullable: true,
                oldClrType: typeof(int),
                oldType: "int");

            migrationBuilder.AddForeignKey(
                name: "FK_Payments_PaymentComments_PaymentCommentId",
                table: "Payments",
                column: "PaymentCommentId",
                principalTable: "PaymentComments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Payments_PaymentComments_PaymentCommentId",
                table: "Payments");

            migrationBuilder.AlterColumn<int>(
                name: "PaymentCommentId",
                table: "Payments",
                type: "int",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Payments_PaymentComments_PaymentCommentId",
                table: "Payments",
                column: "PaymentCommentId",
                principalTable: "PaymentComments",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }
    }
}
