﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Expenses.Migrations
{
    public partial class Initial5 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Amount",
                table: "Limits");

            migrationBuilder.AddColumn<double>(
                name: "BottomAmount",
                table: "Limits",
                nullable: false,
                defaultValue: 0.0);

            migrationBuilder.AddColumn<double>(
                name: "TopAmount",
                table: "Limits",
                nullable: false,
                defaultValue: 0.0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "BottomAmount",
                table: "Limits");

            migrationBuilder.DropColumn(
                name: "TopAmount",
                table: "Limits");

            migrationBuilder.AddColumn<double>(
                name: "Amount",
                table: "Limits",
                type: "double",
                nullable: false,
                defaultValue: 0.0);
        }
    }
}
