﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Expenses.Migrations
{
    public partial class MigrationDayInMonth : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "FirstDayInMonth",
                table: "Users",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "FirstDayInMonth",
                table: "Users");
        }
    }
}
