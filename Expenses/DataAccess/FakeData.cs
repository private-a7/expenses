﻿using Expenses.Model;
using Expenses.MySQLRepository;
using Expenses.Repository;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Expenses.DataAccess
{
    public class FakeData
    {

        public static void Seed(IServiceProvider provider, IConfiguration config)
        {
            using (var scope = provider.CreateScope())
            {
                var context = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
                context.Database.EnsureDeleted();
                context.Database.EnsureCreated();

                var users = scope.ServiceProvider.GetRequiredService<IUserRepository>();
                var payments = scope.ServiceProvider.GetRequiredService<IRepository<Payment>>();
                var groupRepo = scope.ServiceProvider.GetRequiredService<IRepository<PaymentGroup>>();

                List<PaymentGroup> groups = new List<PaymentGroup>();


                User u = new User() { Email = "test@example.com", PaymentGroup = new List<PaymentGroup>() };
                users.Save(u);
                PaymentGroup g = new PaymentGroup() { Name = "Grupa", Color = Color.Red.ToArgb(), User = u, UserId = u.Id  };
                groups.Add(g);
                groupRepo.Save(g);
                Shop shop = new Shop() { Name = "pijaca", Group = g, GroupId = g.Id };
                u.PaymentGroup.Add(g);
                g = new PaymentGroup() { Name = "Grupa 2", Color = Color.Green.ToArgb(), User = u, UserId = u.Id };
                groups.Add(g);
                groupRepo.Save(g);
                u.PaymentGroup.Add(g);
                g = new PaymentGroup() { Name = "Grupa 3", Color = Color.Bisque.ToArgb(), User = u, UserId = u.Id };
                groups.Add(g);
                groupRepo.Save(g);
                u.PaymentGroup.Add(g);

                users.Save(u);



                Payment p = new Payment()
                {
                    UserId = u.Id,
                    Items = new List<PaymentItem>()
                    {
                        new PaymentItem()
                        {
                            Description = "voce",
                            Amount = 10329,
                        }
                    },
                    Shop = shop,
                    PaymentMethod = new PaymentMethod() { Method = "kes"},
                    PurchaseDate = DateTime.Now
                };

                payments.Save(p);
            }
        }
    }
}
