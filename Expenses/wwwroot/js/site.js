﻿// Please see documentation at https://docs.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.

function formatMoney(amount, addCurrency = true) {
	return Number(amount).toFixed(2) + (addCurrency === true ? " din" : "");
}

// Needed because toJSON prints UTC time,
// which can be different from current time
// (1.2. 00:00 +0200 in UTC is 31.1. 22:00)
function dateToJSON(jsonDate) {
	let d = new Date(jsonDate);
	d = new Date(d.getTime() - (d.getTimezoneOffset() * 60000)).toJSON();
	return d;
}

// Pads value with zeroes.
function padZero(value, width = 2) {
	let s = ("000000000" + value).slice(-width);
	return s;
}

function displayDateTime(date) {
	return date.getDate() + "-" + padZero(date.getMonth() + 1) + "-" + date.getFullYear() +
		" " + padZero(date.getHours()) + ":" + padZero(date.getMinutes());
}

function getChartColor(index) {
	index = index % chartColors.length;
	return chartColors[index];
}

function distinct(array) {
	let el = [];
	$.each(array, (index, val) => {
		if (el.indexOf(val) === -1)
			el.push(val);
	});
	return el;
}

chartColors = [
	"red",
	"blue",
	"yellow",
	"green",
	"purple",
	"orange",
];